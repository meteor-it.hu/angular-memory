import { ChangeDetectionStrategy, Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';

@Component({
  selector: 'app-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SelectComponent {
	@Input() label: string;
	@Input() options: any[];
	@ViewChild('cardSelect') select: ElementRef;
	@Output() selectChanged = new EventEmitter

	onSelectChange(): void {
		this.selectChanged.emit(this.select.nativeElement.value);
	}
}
